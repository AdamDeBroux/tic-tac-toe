#pragma once
#include <iostream>
using namespace std;
class TicTacToe {
private:
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;
public:
	TicTacToe() {
		for (int x = 0; x < 9; x++) {
			m_board[x] = '_';
		}
		m_numTurns = 0;
		m_playerTurn = '1';
		m_winner = ' ';
	}
	void DisplayBoard() {
		cout << m_board[0] << m_board[1] << m_board[2] << "\n" << m_board[3] << m_board[4] << m_board[5] << "\n" << m_board[6] << m_board[7] << m_board[8] << "\n";

	}
	bool IsOver() {
		if (m_board[0] == 'X' && m_board[1] == 'X' && m_board[2] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[3] == 'X' && m_board[4] == 'X' && m_board[5] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[6] == 'X' && m_board[7] == 'X' && m_board[8] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[0] == 'X' && m_board[3] == 'X' && m_board[6] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[1] == 'X' && m_board[4] == 'X' && m_board[7] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[0] == 'X' && m_board[4] == 'X' && m_board[8] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[2] == 'X' && m_board[4] == 'X' && m_board[6] == 'X') {
			m_winner = 1;
			return true;
		}
		else if (m_board[0] == 'O' && m_board[1] == 'O' && m_board[2] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_board[3] == 'O' && m_board[4] == 'O' && m_board[5] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_board[6] == 'O' && m_board[7] == 'O' && m_board[8] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_board[0] == 'O' && m_board[3] == 'O' && m_board[6] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_board[1] == 'O' && m_board[4] == 'O' && m_board[7] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_board[0] == 'O' && m_board[4] == 'O' && m_board[8] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_board[2] == 'O' && m_board[4] == 'O' && m_board[6] == 'O') {
			m_winner = 2;
			return true;
		}
		else if (m_numTurns == 9) return true;
		return false;
	}
	char GetPlayerTurn() {return m_playerTurn;}
	bool IsValidMove(int position) {
		if (m_board[position-1] == '_') return true;
		return false;
	}
	void Move(int position) {
		if (m_playerTurn == '1') {
			m_board[position-1] = 'X';
			m_playerTurn = '2';
		}
		else {
			m_board[position-1] = 'O';
			m_playerTurn = '1';
		}
		m_numTurns++;
	}
	void DisplayResult() {
		if (m_winner == 1) cout << "Player 1 is the winner!\n";
		else if (m_winner == 2) cout << "Player 2 is the winner!\n";
		else cout << "It's a draw!\n";
	}
};